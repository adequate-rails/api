# Adequate Rails API Template

Provides adequate **TDD** setup for modern **Rails** application with **Gitlab CI** integrated.

# Adequate Rails Template

Instead of cloning this repository and editing, you can directly use this command to create your new project interactively.

```rails new my-api --api -m https://gitlab.com/adequate-rails/template/raw/master/adequate.rb```

---

## Rubocop

[Rubocop] (https://rubocop.readthedocs.io) is ruby style guide code analyzer which is almost industry standard.

I've added some configuration, if you break standards, you will get Cop name and related documentation link for easier fixes.

---

## Brakeman

[Brakeman] (https://github.com/presidentbeef/brakeman) is a static analysis security vulnerability scanner for Ruby on Rails applications.

---

## Guard

[Guard] (https://github.com/guard/guard) intelligently watches project for any changes and run certain commands instantly. Since not all tests, checks run for entire repo, this saves you time because only particular tests, checks (intelligently) will - Run following some rules. These rules explained below.

All those gems are cool but guard makes them 10x cooler. Guard puts all this things together and packs for the sake of TDD.

* [guard-minitest] (https://github.com/guard/guard-minitest)

    Beside default setup, those are rules followed.

    - Run related controller test when model updated.
    - Run related model test when any fixture updated.
    - Run all model tests when ```application_record``` updated.
    - Run all model tests when ```schema.rb``` updated.
    - Run related job test when any job updated.
    - Run all job tests if ```application_jobs``` updated.

* [guard-rubocop] (https://github.com/yujinakayama/guard-rubocop)

    Guard rubocop works straightforward. When you update a file, it checks file for style offenses.

* [guard-brakeman] (https://github.com/guard/guard-brakeman)

    If you ever write insecure code, this will warn you instantly.

---

## Gitlab CI

[Gitlab CI] (https://docs.gitlab.com/ce/ci/quick_start/README.html) is awesome built-in continuous integration system, you can have CI run for your every single push for free.

Adequate setup for Gitlab CI is implemented. You may need to do followings to make it work.

  - Go to your repository's project variables. This must be something like https://gitlab.com/username/project/variables

  - Add the following ENV's.
    * Key: PG_HOST Value: postgres
    * Key: PG_USER Value: runner

On every push CI will:

- Drop database, create new one, load schema, migrate,
- run rails tests,
- run rubocop,
- run brakeman

This is it! Now you ready to see sweet green tick with confidence.


## Gitlab

While this setup provides you many things, you may need to have a work flow for project management. Gitlab has great tutorials for this. You can start with this one.

[Gitlab Flow] (https://docs.gitlab.com/ce/university/training/gitlab_flow.html)

You may also want to take a look at [Gitlab University] (https://docs.gitlab.com/ce/university/README.html)

---

![alt/text] (https://willi.co/adequate-rails/adequate-rails-console.png)

![alt/text] (https://willi.co/adequate-rails/adequate-rails-gitlab-ci.png)
